# imexporter

#### 项目介绍
报表的导入导出工具类，报表导出的抽象框架

项目已经发布至 `jcenter`、`mavenCentral` （1.8.3之前）和jitpack（v1.8.4开始）

【mavenCentral和jcenter】
compile 'top.jfunc.report:imexporter:${version}'

【jitpack】[![](https://jitpack.io/v/com.gitee.xxssyyyyssxx/imexporter.svg)](https://jitpack.io/#com.gitee.xxssyyyyssxx/imexporter)
maven { url 'https://jitpack.io' }
compile 'com.gitee.xxssyyyyssxx:imexporter:${version}'